package main;

import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.Color;

import java.awt.Image;
import java.awt.image.BufferedImage;

public class Renderer extends JPanel
{
    public static Keyboard keyboard = new Keyboard();
    
    public Renderer()
    {
        this.addKeyListener(keyboard);
        this.setBackground(Color.black);
        this.setDoubleBuffered(true);
        this.setFocusable(true);
    }
    
    @Override
    public void paintComponent(Graphics ge)
    {
        BufferedImage bi = new BufferedImage(640, 480, BufferedImage.TYPE_INT_RGB);
        Graphics g = bi.getGraphics(); // draw everything to a bufferedimage
        
        // draw the resized image to the screen
        ge.drawImage(bi.getScaledInstance(Main.w.getSize().width, Main.w.getSize().height, Image.SCALE_SMOOTH), 0, 0, null);
    }
}