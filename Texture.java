package main;

// class for storing textures

import java.awt.Image;
import javax.swing.ImageIcon;

public class Texture
{
    Main obj = new Main();
    
    // every texture name should be uppercase. it should be public static final Image
    public static final Image BRICK = new ImageIcon(obj.getClass().getResource("/resources/brick.png")).getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
}