package main;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

public class Keyboard implements KeyListener
{
    public boolean K_UP;
    public boolean K_DOWN;
    public boolean K_LEFT;
    public boolean K_RIGHT;
    
    public boolean K_ENTER;
    public boolean K_SPACE;
    public boolean K_ESCAPE;
    
    public boolean K_A;
    public boolean K_B;
    public boolean K_C;
    public boolean K_D;
    public boolean K_E;
    public boolean K_F;
    public boolean K_G;
    public boolean K_H;
    public boolean K_I;
    public boolean K_J;
    public boolean K_K;
    public boolean K_L;
    public boolean K_M;
    public boolean K_N;
    public boolean K_O;
    public boolean K_P;
    public boolean K_Q;
    public boolean K_R;
    public boolean K_S;
    public boolean K_T;
    public boolean K_U;
    public boolean K_V;
    public boolean K_W;
    public boolean K_X;
    public boolean K_Y;
    public boolean K_Z;
    
    public boolean K_0;
    public boolean K_1;
    public boolean K_2;
    public boolean K_3;
    public boolean K_4;
    public boolean K_5;
    public boolean K_6;
    public boolean K_7;
    public boolean K_8;
    public boolean K_9;
    
    @Override
    public void keyTyped(KeyEvent e)
    {
    }
    @Override
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_UP) { K_UP = true; }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) { K_DOWN = true; }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) { K_LEFT = true; }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) { K_RIGHT = true; }
        
        if (e.getKeyCode() == KeyEvent.VK_ENTER) { K_ENTER = true; }
        if (e.getKeyCode() == KeyEvent.VK_SPACE) { K_SPACE = true; }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) { K_ESCAPE = true; }
        
        if (e.getKeyCode() == KeyEvent.VK_A) { K_A = true; }
        if (e.getKeyCode() == KeyEvent.VK_B) { K_B = true; }
        if (e.getKeyCode() == KeyEvent.VK_C) { K_C = true; }
        if (e.getKeyCode() == KeyEvent.VK_D) { K_D = true; }
        if (e.getKeyCode() == KeyEvent.VK_E) { K_E = true; }
        if (e.getKeyCode() == KeyEvent.VK_F) { K_F = true; }
        if (e.getKeyCode() == KeyEvent.VK_G) { K_G = true; }
        if (e.getKeyCode() == KeyEvent.VK_H) { K_H = true; }
        if (e.getKeyCode() == KeyEvent.VK_I) { K_I = true; }
        if (e.getKeyCode() == KeyEvent.VK_J) { K_J = true; }
        if (e.getKeyCode() == KeyEvent.VK_K) { K_K = true; }
        if (e.getKeyCode() == KeyEvent.VK_L) { K_L = true; }
        if (e.getKeyCode() == KeyEvent.VK_M) { K_M = true; }
        if (e.getKeyCode() == KeyEvent.VK_N) { K_N = true; }
        if (e.getKeyCode() == KeyEvent.VK_O) { K_O = true; }
        if (e.getKeyCode() == KeyEvent.VK_P) { K_P = true; }
        if (e.getKeyCode() == KeyEvent.VK_Q) { K_Q = true; }
        if (e.getKeyCode() == KeyEvent.VK_R) { K_R = true; }
        if (e.getKeyCode() == KeyEvent.VK_S) { K_S = true; }
        if (e.getKeyCode() == KeyEvent.VK_T) { K_T = true; }
        if (e.getKeyCode() == KeyEvent.VK_U) { K_U = true; }
        if (e.getKeyCode() == KeyEvent.VK_V) { K_V = true; }
        if (e.getKeyCode() == KeyEvent.VK_W) { K_W = true; }
        if (e.getKeyCode() == KeyEvent.VK_X) { K_X = true; }
        if (e.getKeyCode() == KeyEvent.VK_Y) { K_Y = true; }
        if (e.getKeyCode() == KeyEvent.VK_Z) { K_Z = true; }
        
        if (e.getKeyCode() == KeyEvent.VK_0) { K_0 = true; }
        if (e.getKeyCode() == KeyEvent.VK_1) { K_1 = true; }
        if (e.getKeyCode() == KeyEvent.VK_2) { K_2 = true; }
        if (e.getKeyCode() == KeyEvent.VK_3) { K_3 = true; }
        if (e.getKeyCode() == KeyEvent.VK_4) { K_4 = true; }
        if (e.getKeyCode() == KeyEvent.VK_5) { K_5 = true; }
        if (e.getKeyCode() == KeyEvent.VK_6) { K_6 = true; }
        if (e.getKeyCode() == KeyEvent.VK_7) { K_7 = true; }
        if (e.getKeyCode() == KeyEvent.VK_8) { K_8 = true; }
        if (e.getKeyCode() == KeyEvent.VK_9) { K_9 = true; }
    }
    @Override
    public void keyReleased(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_UP) { K_UP = false; }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) { K_DOWN = false; }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) { K_LEFT = false; }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) { K_RIGHT = false; }
        
        if (e.getKeyCode() == KeyEvent.VK_ENTER) { K_ENTER = false; }
        if (e.getKeyCode() == KeyEvent.VK_SPACE) { K_SPACE = false; }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) { K_ESCAPE = false; }
        
        if (e.getKeyCode() == KeyEvent.VK_A) { K_A = false; }
        if (e.getKeyCode() == KeyEvent.VK_B) { K_B = false; }
        if (e.getKeyCode() == KeyEvent.VK_C) { K_C = false; }
        if (e.getKeyCode() == KeyEvent.VK_D) { K_D = false; }
        if (e.getKeyCode() == KeyEvent.VK_E) { K_E = false; }
        if (e.getKeyCode() == KeyEvent.VK_F) { K_F = false; }
        if (e.getKeyCode() == KeyEvent.VK_G) { K_G = false; }
        if (e.getKeyCode() == KeyEvent.VK_H) { K_H = false; }
        if (e.getKeyCode() == KeyEvent.VK_I) { K_I = false; }
        if (e.getKeyCode() == KeyEvent.VK_J) { K_J = false; }
        if (e.getKeyCode() == KeyEvent.VK_K) { K_K = false; }
        if (e.getKeyCode() == KeyEvent.VK_L) { K_L = false; }
        if (e.getKeyCode() == KeyEvent.VK_M) { K_M = false; }
        if (e.getKeyCode() == KeyEvent.VK_N) { K_N = false; }
        if (e.getKeyCode() == KeyEvent.VK_O) { K_O = false; }
        if (e.getKeyCode() == KeyEvent.VK_P) { K_P = false; }
        if (e.getKeyCode() == KeyEvent.VK_Q) { K_Q = false; }
        if (e.getKeyCode() == KeyEvent.VK_R) { K_R = false; }
        if (e.getKeyCode() == KeyEvent.VK_S) { K_S = false; }
        if (e.getKeyCode() == KeyEvent.VK_T) { K_T = false; }
        if (e.getKeyCode() == KeyEvent.VK_U) { K_U = false; }
        if (e.getKeyCode() == KeyEvent.VK_V) { K_V = false; }
        if (e.getKeyCode() == KeyEvent.VK_W) { K_W = false; }
        if (e.getKeyCode() == KeyEvent.VK_X) { K_X = false; }
        if (e.getKeyCode() == KeyEvent.VK_Y) { K_Y = false; }
        if (e.getKeyCode() == KeyEvent.VK_Z) { K_Z = false; }
        
        if (e.getKeyCode() == KeyEvent.VK_0) { K_0 = false; }
        if (e.getKeyCode() == KeyEvent.VK_1) { K_1 = false; }
        if (e.getKeyCode() == KeyEvent.VK_2) { K_2 = false; }
        if (e.getKeyCode() == KeyEvent.VK_3) { K_3 = false; }
        if (e.getKeyCode() == KeyEvent.VK_4) { K_4 = false; }
        if (e.getKeyCode() == KeyEvent.VK_5) { K_5 = false; }
        if (e.getKeyCode() == KeyEvent.VK_6) { K_6 = false; }
        if (e.getKeyCode() == KeyEvent.VK_7) { K_7 = false; }
        if (e.getKeyCode() == KeyEvent.VK_8) { K_8 = false; }
        if (e.getKeyCode() == KeyEvent.VK_9) { K_9 = false; }
    }
}
