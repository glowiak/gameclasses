package main;

// this is a template of game Main class
// included:
// -window
// -timer
// -fps counter
// -fps limiter

// the timer is based on the one Notch's Minicraft uses, but with nanoseconds instead of milliseconds and has an fps limiter

// when using in your project, please remove all these comments.

import javax.swing.JFrame;

public class Main implements Runnable
{
    public static JFrame w = new JFrame("Name of your game");
    public static Renderer r = new Renderer(); // name of your JPanel extended class
    
    public static final int WIDTH = 640;
    public static final int HEIGHT = 480;
    
    public static int fps = 0;
    public static final int wantedFPS = 60; // I usually go with 333 or 315, but 60 is more popular
    
    public boolean IS_RUNNING = true;
    
    public static void main(String[] args)
    {
        new Thread(new Main()).start();
    }
    
    @Override
    public void run()
    {
        w.setSize(WIDTH, HEIGHT);
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.add(r);
        w.setVisible(true);
        
        double drawInterval = 1000000000/wantedFPS; // 1000000000 is a second

        boolean shouldRender = false;
        long lastTime = System.nanoTime();
        long staticTime = System.nanoTime();
        long SECOND = 1000000000L;
        double unprocessed = 0.0;
        int frames = 0;
        double nextDrawTime = lastTime + drawInterval;
        
        while (this.IS_RUNNING)
        {
            long currentTime = System.nanoTime();
            long passedTime = currentTime - lastTime;

            double remainingTime = nextDrawTime - currentTime;
            remainingTime = remainingTime/1000000;

            if (currentTime - staticTime > SECOND)
            {
                fps = frames;
                frames = 0;
                staticTime += SECOND;
            }
            lastTime = currentTime;
            unprocessed += passedTime/(double)SECOND;
            
            while (unprocessed >= 1)
            {
                shouldRender = true;
                unprocessed -= 1;
            }
            
            nextDrawTime += drawInterval;
            if (remainingTime < 0) { remainingTime = 0; }
            try { Thread.sleep((long)remainingTime); } catch (InterruptedException e) { e.printStackTrace(); }
            
            if (shouldRender)
            {
                frames++;
                // place your game code here
            }
        }
    }
}