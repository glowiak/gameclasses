# gameclasses

java classes useful for making games

### List

1. main.Keyboard - a simplified version of java.awt.event.KeyListener, use with (name of jpanel/panel).addKeyListener(object_of_Keyboard); provides simple booleans starting with K_ for each letter and number.

2. main.Mouse - a class for getting mouse position and buttons pressed. Provides booleans for each mouse button (three of them) starting with 'MB_'. Also has functions to get mouse x and y, but you need first to provide a base JFrame (Mouse.setFrame(JFrame frame)) to count position from.

3. main.Entity - abstract entity class

4. main.Main - game starting class + a timer

5. main.Renderer - extended from JPanel to draw size-independent game screen

6. main.Texture - class for managing textures

7. main.State - class for managing game states