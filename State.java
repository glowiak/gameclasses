package main;

// class for storing game states

public class State
{
    // adjust as you want, here are examples
    
    public static final int LOADING = 0;
    public static final int MENU = 1;
    public static final int OPTIONS = 2;
    public static final int WORLDS = 3;
    public static final int GAME = 4;
    
    public static final CURRENT = State.LOADING;
}